﻿
namespace CompilerScanner
{
    partial class tokensForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tokensGridView = new System.Windows.Forms.DataGridView();
            this.toMainFormButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tokensGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tokensGridView
            // 
            this.tokensGridView.AllowUserToAddRows = false;
            this.tokensGridView.AllowUserToDeleteRows = false;
            this.tokensGridView.AllowUserToOrderColumns = true;
            this.tokensGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tokensGridView.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tokensGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.tokensGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tokensGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.tokensGridView.Location = new System.Drawing.Point(9, 10);
            this.tokensGridView.Margin = new System.Windows.Forms.Padding(2);
            this.tokensGridView.Name = "tokensGridView";
            this.tokensGridView.ReadOnly = true;
            this.tokensGridView.RowHeadersWidth = 51;
            this.tokensGridView.RowTemplate.Height = 24;
            this.tokensGridView.Size = new System.Drawing.Size(780, 427);
            this.tokensGridView.TabIndex = 0;
            // 
            // toMainFormButton
            // 
            this.toMainFormButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.toMainFormButton.Location = new System.Drawing.Point(12, 442);
            this.toMainFormButton.Name = "toMainFormButton";
            this.toMainFormButton.Size = new System.Drawing.Size(75, 23);
            this.toMainFormButton.TabIndex = 1;
            this.toMainFormButton.Text = "<---";
            this.toMainFormButton.UseVisualStyleBackColor = true;
            this.toMainFormButton.Click += new System.EventHandler(this.toMainFormButton_Click);
            // 
            // tokensForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 473);
            this.Controls.Add(this.toMainFormButton);
            this.Controls.Add(this.tokensGridView);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(249, 198);
            this.Name = "tokensForm";
            this.Text = "Tokens List";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.tokensForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tokensGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tokensGridView;
        private System.Windows.Forms.Button toMainFormButton;
    }
}