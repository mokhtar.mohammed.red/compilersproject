﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompilerScanner
{
    public partial class tokensForm : Form
    {

        String text;
        public tokensForm()
        {
            InitializeComponent();
        }

        private void tokensForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;

        }

        public void setTextBox (String givenText)
        {
            text = givenText;
        }
        public void updateDataGridView(List<MainForm.Token> tokenList) {
            BindingSource source = new BindingSource(tokenList, null);
            tokensGridView.DataSource = source;
            tokensGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            tokensGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            tokensGridView.AutoResizeRows(DataGridViewAutoSizeRowsMode.DisplayedCells);
            tokensGridView.Columns["tokenVal"].DisplayIndex = 0;
            tokensGridView.Columns["type"].DisplayIndex = 1;
        }

        private void toMainFormButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm f = new MainForm(text);
            f.ShowDialog();
        }
    }
}
