﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompilerScanner
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        public MainForm(String Text)
        {
            InitializeComponent();
            codeTextbox.Text = Text;
        }
        public enum TokenType
        {
            ReservedWord,                               //= "Reserved word",
            AdditionOperator,                           //="Addition operator",
            SubtractOperator,                           //="Subtract operator",
            MultiplicationOperator,                     //="Multiplication operator",
            DivisionOperator,                           //="Division operator",
            EqualOperator,                              //="Equal operator",
            LessthanOperator,                           //="Less than operator",
            GreaterthanOperator,                        //="Greater than operator",
            Openbracket,                                //="Open bracket",
            Closedbracket,                              //="Closed bracket",
            SemicolonOperator,                          //="Semicolon operator",
            AssignOperator,                             //="Assign operator",
            Number,                                     //="Number",
            Identifier,                                //="Identifier",
            Garbage
        }

        public class Token
        {
            
            string tokenVal;
            TokenType type;

            public TokenType Type { get => type; set => type = value; }
            public string TokenVal { get => tokenVal; set => tokenVal = value; }

            public Token(TokenType type, string tokenVal)
            {
                this.type = type;
                this.tokenVal = tokenVal;
            }
            public void printTokenVal()
            {
                Console.WriteLine(tokenVal);
            }
            public TokenType getTokenType()
            {
                return type;
            }

        }
        private static Token scanNextToken(string S, ref int startPos)
        {
            Token X;
            String temp = "";

            if (S[startPos] == '{')
            {
                startPos++;
                while (startPos < S.Length) // deals with comments
                {
                    if (S[startPos] == '}')
                    {
                        startPos++;
                        break;
                    }
                    startPos++;
                    if (startPos == S.Length)
                    {
                        MessageBox.Show("Invalid comment \n\"{\" must have a matching  \"}\"", "Invalid Comment", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Console.WriteLine("Invalid comment \n\"{\" must have a matching \"}\"");
                        Environment.Exit(0);
                    }
                }
            }

            while (startPos < S.Length) // deals with whitespace
            {
                if (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\r' || S[startPos] == '\t')
                    startPos++;
                else
                    break;
            }

            if (startPos < S.Length)
            {
                switch (S[startPos])
                {
                    case char c when (c >= '0' && c <= '9'):
                        temp += S[startPos];
                        startPos++;
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if (S[i] >= '0' && S[i] <= '9')
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Number, temp);
                        return X;
                    case '+':
                        X = new Token(TokenType.AdditionOperator, "+");
                        startPos++;
                        return X;
                    case '-':
                        X = new Token(TokenType.SubtractOperator, "-");
                        startPos++;
                        return X;
                    case '*':
                        X = new Token(TokenType.MultiplicationOperator, "*");
                        startPos++;
                        return X;
                    case '/':
                        X = new Token(TokenType.DivisionOperator, "/");
                        startPos++;
                        return X;
                    case '=':
                        X = new Token(TokenType.EqualOperator, "=");
                        startPos++;
                        return X;
                    case '<':
                        X = new Token(TokenType.LessthanOperator, "<");
                        startPos++;
                        return X;
                    case '>':
                        X = new Token(TokenType.GreaterthanOperator, ">");
                        startPos++;
                        return X;
                    case '(':
                        X = new Token(TokenType.Openbracket, "(");
                        startPos++;
                        return X;
                    case ')':
                        X = new Token(TokenType.Closedbracket, ")");
                        startPos++;
                        return X;
                    case ';':
                        X = new Token(TokenType.SemicolonOperator, ";");
                        startPos++;
                        return X;
                    case ':':
                        try
                        {
                            if (S[startPos + 1] == '=')
                            {
                                X = new Token(TokenType.AssignOperator, ":=");
                                startPos = startPos + 2;
                                return X;
                            }
                        }
                        catch (IndexOutOfRangeException error)
                        {
                        }
                        X = new Token(TokenType.Garbage, ":");
                        startPos++;
                        return X;
                    case 'i':
                        temp = "i";
                        startPos++;
                        try
                        {
                            if ((S[startPos] == 'f' && startPos == S.Length - 1) || (S[startPos] == 'f' && (S[startPos+1] == ' ' || S[startPos+1] == '\n' || S[startPos+1] == '\t' || S[startPos+1] == '\r')))
                            {
                                X = new Token(TokenType.ReservedWord, "if");
                                startPos++;
                                return X;
                            }
                        }
                        catch (IndexOutOfRangeException error)
                        {
                        }
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if ((S[i] >= 'a' && S[i] <= 'z') || (S[i] >= 'A' && S[i] <= 'Z'))
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Identifier, temp);
                        return X;
                    case 't':
                        temp = "t";
                        startPos++;
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if (((temp == "then") && (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\t' || S[startPos] == '\r')) || (temp == "the" && S[startPos] == 'n' && (i == S.Length - 1)))
                            {
                                X = new Token(TokenType.ReservedWord, "then");
                                startPos++;
                                return X;
                            }
                            if ((S[i] >= 'a' && S[i] <= 'z') || (S[i] >= 'A' && S[i] <= 'Z'))
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Identifier, temp);
                        return X;
                    case 'e':
                        temp = "e";
                        startPos++;
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if (((temp == "else") && (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\t' || S[startPos] == '\r' )) || (temp =="els" && S[startPos] == 'e' && (i == S.Length - 1)))
                            {
                                X = new Token(TokenType.ReservedWord, "else");
                                startPos++;
                                return X;
                            }
                            else if (((temp == "end") && (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\t' || S[startPos] == '\r')) || (temp == "en" && S[startPos] == 'd' && (i == S.Length - 1)))
                            {
                                X = new Token(TokenType.ReservedWord, "end");
                                startPos++;
                                return X;
                            }
                            else if ((S[i] >= 'a' && S[i] <= 'z') || (S[i] >= 'A' && S[i] <= 'Z'))
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Identifier, temp);
                        return X;
                    case 'r':
                        temp = "r";
                        startPos++;
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if (((temp == "repeat") && (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\t' || S[startPos] == '\r')) || (temp == "repea" && S[startPos] == 't' && (i == S.Length - 1)))
                            {
                                X = new Token(TokenType.ReservedWord, "repeat");
                                startPos++;
                                return X;
                            }
                            if (((temp == "read") && (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\t' || S[startPos] == '\r')) || (temp == "rea" && S[startPos] == 'd' && (i == S.Length - 1)))
                            {
                                X = new Token(TokenType.ReservedWord, "read");
                                startPos++;
                                return X;
                            }
                            if ((S[i] >= 'a' && S[i] <= 'z') || (S[i] >= 'A' && S[i] <= 'Z'))
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Identifier, temp);
                        return X;
                    case 'u':
                        temp = "u";
                        startPos++;
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if (((temp == "until") && (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\t' || S[startPos] == '\r')) || (temp == "unti" && S[startPos] == 'l' && (i == S.Length - 1)))
                            {
                                X = new Token(TokenType.ReservedWord, "until");
                                startPos++;
                                return X;
                            }
                            if ((S[i] >= 'a' && S[i] <= 'z') || (S[i] >= 'A' && S[i] <= 'Z'))
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Identifier, temp);
                        return X;
                    case 'w':
                        temp = "w";
                        startPos++;
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if (((temp == "write") && (S[startPos] == ' ' || S[startPos] == '\n' || S[startPos] == '\t' || S[startPos] == '\r')) || (temp == "writ" && S[startPos] == 'e' && (i == S.Length - 1)))
                            {
                                X = new Token(TokenType.ReservedWord, "write");
                                startPos++;
                                return X;
                            }
                            if ((S[i] >= 'a' && S[i] <= 'z') || (S[i] >= 'A' && S[i] <= 'Z'))
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Identifier, temp);
                        return X;
                    case char c when (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'):
                        temp += S[startPos];
                        startPos++;
                        for (int i = startPos; i < S.Length; i++)
                        {
                            if ((S[i] >= 'a' && S[i] <= 'z') || (S[i] >= 'A' && S[i] <= 'Z'))
                            {
                                temp += S[i];
                                startPos++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        X = new Token(TokenType.Identifier, temp);
                        return X;
                    case '{':
                        X = new Token(TokenType.Garbage, "");
                        return X;
                    case '}':
                        X = new Token(TokenType.Garbage, "");
                        return X;

                    default:
                        X = new Token(TokenType.Garbage, Char.ToString(S[startPos]));
                        break;
                }
            }
            try
            {
                X = new Token(TokenType.Garbage, Char.ToString(S[startPos]));
            }
            catch (IndexOutOfRangeException error)
            {
                X = new Token(TokenType.Garbage,"");
            }
            startPos++;
            return X;
        }
        private static List<Token> getTokens(String S)
        {
            List<Token> tokenlist = new List<Token>();
            int startPos = 0;
            while (startPos < S.Length)
            {
                Token temp = scanNextToken(S, ref startPos);
                if (TokenType.Garbage != temp.getTokenType())
                    tokenlist.Add(temp);
                else if (temp.TokenVal != "{" && temp.TokenVal != "}" && temp.TokenVal != " " && temp.TokenVal != "\r" && temp.TokenVal != "\t" && temp.TokenVal != "\n" && temp.TokenVal != "")
                {
                    MessageBox.Show("Token \"" + temp.TokenVal + "\" is invalid in tiny language", "Invalid Token", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
            }
            return tokenlist;
        }


        private void scanButton_Click(object sender, EventArgs e)
        {
            List<Token> temp = getTokens(codeTextbox.Text);
            if ( temp != null) { 
            this.Hide();
            var TokenForm = new tokensForm();
            TokenForm.updateDataGridView(temp);
            TokenForm.setTextBox(codeTextbox.Text);
            TokenForm.Show();
        }
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
           codeTextbox.Lines = File.ReadAllLines(openFileDialog.FileName);
        }
    }
}
