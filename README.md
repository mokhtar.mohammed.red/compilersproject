# Project Description
> This project was created for the "Design of Compilers" course, which tasked students with creating a scanner for Tiny Language. ([The definition of Tiny Language can be found here.](http://jlu.myweb.cs.uwindsor.ca/214/language.htm))
>
> The scanner was developed using C# and a UI was built using WinForms to display the tokens extracted from the code.
